Денис,

нужно сделать простой магазин на зенд фреймворк
главная страница с категориями, страница категории с товарами, страница товара с кнопкой Купить, корзина с оформлением заказа.

админка для товаров, категорий, заказов
дизайн не требуется, можно просто использовать бутстрап

Код должен соответствовать стандартам для Zend framework 2.
код вместе с базой залить на битбакет, вместе с инструкцией по разворачиванию


php composer.phar create-project --stability="dev" "e:\SVN\Programs\bitbucket\zend_shop\"

CREATE TABLE category (id int(11) NOT NULL auto_increment,title varchar(100) NOT NULL,PRIMARY KEY (id));
INSERT INTO category (title) VALUES ('TV');
INSERT INTO category (title) VALUES ('Smartphone');
INSERT INTO category (title) VALUES ('Notebook');
INSERT INTO category (title) VALUES ('Photo');

CREATE TABLE product (id int(11) NOT NULL auto_increment,title varchar(100) NOT NULL,PRIMARY KEY (id));
INSERT INTO product (title) VALUES ('TV1');
INSERT INTO product (title) VALUES ('TV2');
INSERT INTO product (title) VALUES ('TV3');
INSERT INTO product (title) VALUES ('TV4');
INSERT INTO product (title) VALUES ('Smartphone1');
INSERT INTO product (title) VALUES ('Smartphone2');
INSERT INTO product (title) VALUES ('Smartphone3');
INSERT INTO product (title) VALUES ('Smartphone4');
INSERT INTO product (title) VALUES ('Notebook1');
INSERT INTO product (title) VALUES ('Notebook2');
INSERT INTO product (title) VALUES ('Notebook3');
INSERT INTO product (title) VALUES ('Notebook4');
INSERT INTO product (title) VALUES ('Photo1');
INSERT INTO product (title) VALUES ('Photo2');
INSERT INTO product (title) VALUES ('Photo3');
INSERT INTO product (title) VALUES ('Photo4');


vendor\bin\doctrine-module orm:info
vendor\bin\doctrine-module orm:validate-schema
vendor\bin\doctrine-module orm:schema-tool:create

module/Application/src/Application/Controller/IndexController.php
public function indexAction() {
    $objectManager = $this
        ->getServiceLocator()
        ->get('Doctrine\ORM\EntityManager');

    $user = new \Application\Entity\User();
    $user->setFullName('Marco Pivetta');

    $objectManager->persist($user);
    $objectManager->flush();

    die(var_dump($user->getId())); // yes, I'm lazy
}