<?php
namespace Category\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Category\Model\Category;
use Category\Form\CategoryForm;

class CategoryController extends AbstractActionController
{
    protected $categoryTable;
    
    public function getCategoryTable()
    {
        if (!$this->categoryTable) 
        {
            $sm = $this->getServiceLocator();
            $this->categoryTable = $sm->get('Category\Model\CategoryTable');
        }
        return $this->categoryTable;
    }
    
    public function indexAction()
    {
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $categories = $objectManager
            ->getRepository('\Category\Model\Category')
            ->findAll();

        $view = new ViewModel(array(
            'categories' => $categories,
        ));

        return $view;        
    }
    
    public function viewAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) 
        {
            $this->flashMessenger()->addErrorMessage('Category id doesn\'t set');
            return $this->redirect()->toRoute('blog');
        }

        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $category = $objectManager
            ->getRepository('\Category\Model\Category')
            ->findOneBy(array('id' => $id));

        if (!$category) 
        {
            $this->flashMessenger()->addErrorMessage(sprintf('Category with id %s doesn\'t exists', $id));
            return $this->redirect()->toRoute('blog');
        }
        $products = null;
        
        $products = $objectManager
            ->getRepository('\Product\Model\Product')
            ->findBy(array('category_id' == $category->getId()));
            
        $view = new ViewModel(array(
            'category' => $category->getArrayCopy(),
            'products' => $products,
        ));

        return $view;        
    }
    
    public function addAction()
    {
        $form = new CategoryForm();
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $category = new Category();
            $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) 
            {
                $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

                $category = new \Category\Model\Category();

                $category->exchangeArray($form->getData());

                $objectManager->persist($category);
                $objectManager->flush();
                
                // Redirect to list of albums
                return $this->redirect()->toRoute('category');
            }
        }
        return array('form' => $form);
    }
    
    public function editAction()
    {
        // Create form.
        $form = new \Category\Form\CategoryForm();
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        if (!$request->isPost()) {
            // Check if id and blogpost exists.
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!$id) {
                $this->flashMessenger()->addErrorMessage('Category id doesn\'t set');
                return $this->redirect()->toRoute('blog');
            }
            $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $category = $objectManager
                ->getRepository('\Category\Model\Category')
                ->findOneBy(array('id' => $id));
            if (!$category) {
                $this->flashMessenger()->addErrorMessage(sprintf('Category with id %s doesn\'t exists', $id));
                return $this->redirect()->toRoute('blog');
            }
            // Fill form data.
            $form->bind($category);
            return array('form' => $form);
        }
        else {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
                $data = $form->getData();
                $id = $data['id'];
                try {
                    $category = $objectManager->find('\Category\Model\Category', $id);
                }
                catch (\Exception $ex) {
                    return $this->redirect()->toRoute('category', array(
                        'action' => 'index'
                    ));
                }
                $category->exchangeArray($form->getData());
                $objectManager->persist($category);
                $objectManager->flush();
                $message = 'Category succesfully saved!';
                $this->flashMessenger()->addMessage($message);
                // Redirect to list of blogposts
                return $this->redirect()->toRoute('category');
            }
            else {
                $message = 'Error while saving category';
                $this->flashMessenger()->addErrorMessage($message);
            }
        }        
    }
    
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Category id doesn\'t set');
            return $this->redirect()->toRoute('category');
        }
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                try {
                    $category = $objectManager->find('Category\Model\Category', $id);
                    $objectManager->remove($category);
                    $objectManager->flush();
                }
                catch (\Exception $ex) {
                    $this->flashMessenger()->addErrorMessage('Error while deleting data');
                    return $this->redirect()->toRoute('category', array(
                        'action' => 'index'
                    ));
                }
                $this->flashMessenger()->addMessage(sprintf('Category %d was succesfully deleted', $id));
            }
            return $this->redirect()->toRoute('category');
        }
        return array(
            'id'    => $id,
            'category' => $objectManager->find('Category\Model\Category', $id),
        );      
    }
}