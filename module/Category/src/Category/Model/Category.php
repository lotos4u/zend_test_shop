<?php
namespace Category\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity */
class Category implements InputFilterAwareInterface
{
    //protected $products;
    //protected $assignedProducts;
    
    /**
    * @var int
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
    
    /**
    * @var string
    * @ORM\Column(type="string", length=255, nullable=false)
    */    
    protected $title;
     
    protected $inputFilter;

    
    /**
    * Get id.
    *
    * @return int
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set id.
    *
    * @param int $id
    *
    * @return void
    */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
    * Get title.
    *
    * @return string
    */
    public function getTitle()
    {
        return $this->title;
    }

    /**
    * Set title.
    *
    * @param string $title
    *
    * @return void
    */
    public function setTitle($title)
    {
        $this->title = $title;
    }
/*
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->assignedProducts = new ArrayCollection();
    }
*/
/*    
    public function addProduct($product)
    {
        $this->products[] = $product;
    }
    
    public function assignedToProduct($product)
    {
        $this->assignedProducts[] = $product;
    }
*/    
    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) 
        {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}