<?php
namespace Product\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Category\Model\Category as Category;

/** @ORM\Entity */
class Product implements InputFilterAwareInterface
{  
    /**
    * @var int
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
    
    /**
    * @var string
    * @ORM\Column(type="string", length=255, nullable=false)
    */    
    protected $title;

    /**
    * @var float
    * @ORM\Column(type="float", nullable=false)
    */    
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="Category\Model\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     **/    
    protected $category;
   
    protected $inputFilter;

    
    /**
    * Get id.
    *
    * @return int
    */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set id.
    *
    * @param int $id
    *
    * @return void
    */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
    * Get title.
    *
    * @return string
    */
    public function getTitle()
    {
        return $this->title;
    }

    /**
    * Set title.
    *
    * @param string $title
    *
    * @return void
    */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
    * Get price.
    *
    * @return float
    */
    public function getPrice()
    {
        return $this->price;
    }

    /**
    * Set price.
    *
    * @param $price
    *
    * @return void
    */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
    * Get category.
    */
    public function getCategory()
    {
        return $this->category;
    }

    /**
    * Set category.
    */
    public function setCategory(Category\Model\Category $category)
    {
        //$category->addProduct($this);
        $this->category = $category;
        return $this;
    }
    
    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
        $this->price = (!empty($data['price'])) ? $data['price'] : null;
        //$this->category = (!empty($data['category'])) ? $data['category'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) 
        {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}