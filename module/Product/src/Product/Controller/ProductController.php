<?php
namespace Product\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Product\Model\Product;
use Category\Model\Category;
use Product\Form\ProductForm;

class ProductController extends AbstractActionController
{
    protected $productTable;
    
    public function getProductTable()
    {
        if (!$this->productTable) 
        {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Product\Model\ProductTable');
        }
        return $this->productTable;
    }
    
    public function indexAction()
    {
        return new ViewModel(array(
            'products' => $this->getProductTable()->fetchAll(),
        ));        
    }
    
    public function addAction()
    {
        $form = new ProductForm();
        $objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $form->add(array(
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            /*'name' => 'category',*/
            'name' => 'category_id',
            'options' => array(
                'label' => 'Category',
                'object_manager' => $objectManager,
                'target_class'   => 'Category\Model\Category',
                /*'property'       => 'id',*/
                'property'       => 'title',
                'is_method'      => true,
                'find_method'    => array(
                    /*'name'   => 'findBy',*/
                    'name'   => 'findAll',
                    'params' => array(
                        /*'criteria' => array('id' >= 0),*/
                        'orderBy'  => array('title' => 'ASC'),
                    ),
                ),
            ),
        ));
        
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $product = new Product();
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) 
            {
                //$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

                $product = new \Product\Model\Product();

                $product->exchangeArray($form->getData());

                if(!empty($data['category_id'])){
                    $category_id = $data['category_id'];
                    $category = $objectManager
                        ->getRepository('\Category\Model\Category')
                        ->findOneBy(array('id' => $category_id));
                    $product->setCategory($category);
                }
                
                $objectManager->persist($product);
                $objectManager->flush();
                
                // Redirect to list of albums
                return $this->redirect()->toRoute('product');
            }
        }
        return array('form' => $form);     
    }
    
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) 
        {
            return $this->redirect()->toRoute('product', array(
                'action' => 'add'
            ));
        }
        // Get the product with the specified id. An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try 
        {
            $product = $this->getProductTable()->getProduct($id);
        }
        catch 
        (\Exception $ex) 
        {
            return $this->redirect()->toRoute('product', array(
                'action' => 'index'
            ));
        }
        $form = new ProductForm();
        $form->bind($product);
        $form->get('submit')->setAttribute('value', 'Edit');
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) 
            {
                $this->getProductTable()->saveCategory($product);
                // Redirect to list of categories
                return $this->redirect()->toRoute('product');
            }
        }
        return array(
            'id' => $id,
            'form' => $form,
        );
    }
    
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) 
        {
            return $this->redirect()->toRoute('product');
        }
        $request = $this->getRequest();
        if ($request->isPost()) 
        {
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') 
            {
                $id = (int) $request->getPost('id');
                $this->getCategoryTable()->deleteProduct($id);
            }
            // Redirect to list of categories
            return $this->redirect()->toRoute('product');
        }
        return array(
            'id' => $id,
            'product' => $this->getCategoryTable()->getCategory($id)
        );        
    }
}